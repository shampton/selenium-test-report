const webdriver = require('selenium-webdriver');
const util = require('util');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);
const path = require('path');
const { expect } = require('chai');

async function takeScreenshot(driver, file){
  let image = await driver.takeScreenshot();
  await writeFile(file, image, 'base64');
}

describe('DefaultTest', () => {
    let driver = new webdriver.Builder()
        .withCapabilities(webdriver.Capabilities.chrome())
        .build();

    it('should go to google.com and check title', async function () {
        await driver.get('https://www.google.com');
        await takeScreenshot(driver, path.join(process.env.OUTPUT_DIR || '.', 'google.png'));
        this.test.attachments = ['google.jpg'];
        const title = await driver.getTitle();
        expect(title).to.equal('Google');
    }).timeout(10000);

    after(async () => driver.quit());
});